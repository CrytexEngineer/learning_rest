package com.example.learning_rest;


import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

//Non Rest Controller

/**
 * @RestController public class EmpolyeeController {
 * private final EmployeeRepository repository;
 * <p>
 * public EmpolyeeController(EmployeeRepository repository) {
 * this.repository = repository;
 * }
 * @GetMapping("/employees") List<Employee> all() {
 * return repository.findAll();
 * }
 * @PostMapping("/employees") Employee newEmployee(@RequestBody Employee newEmployee) {
 * return repository.save(newEmployee);
 * }
 * @GetMapping("/employees/{id}") Employee one(@PathVariable Long id) {
 * return repository.findById(id).
 * orElseThrow(() -> new EmployeeNotFoundException(id));
 * }
 * @PutMapping("/employees/{id}") Employee replaceEmployee(@RequestBody Employee newEmpoyee,
 * @PathVariable Long id) {
 * return repository.findById(id).map(
 * employee -> {
 * employee.setName(newEmpoyee.getName());
 * employee.setRole(newEmpoyee.getRole());
 * return repository.save(employee);
 * })
 * .orElseGet(()->{newEmpoyee.setId(id);
 * return repository.save(newEmpoyee);});
 * }
 * @DeleteMapping("employees/{id}") void deleteEmployee(@PathVariable Long id){
 * repository.deleteById(id);
 * }
 * <p>
 * <p>
 * }
 **/

//Rest Controller
@RestController
public class EmployeeController {
    private final EmployeeRepository repository;
    private EmpoyeeModelAssembler assembler;

    public EmployeeController(EmployeeRepository repository, EmpoyeeModelAssembler assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }

    @GetMapping("/employees")
    CollectionModel<EntityModel<Employee>> all() {
        List<EntityModel<Employee>> employees = repository.findAll().stream()
                .map(employee -> assembler.toModel(employee)).collect(Collectors.toList());

        return CollectionModel.of(employees, linkTo(methodOn(EmployeeController.class).all()).withSelfRel());
    }

    @PostMapping("/employees")
    Employee newEmployee(@RequestBody Employee newEmployee) {
        return repository.save(newEmployee);
    }

    @GetMapping("/employees/{id}")
    EntityModel<Employee> one(@PathVariable Long id) {
        Employee employee = repository.findById(id)
                .orElseThrow(() -> new EmployeeNotFoundException(id));
        return assembler.toModel(employee);
    }


    @PutMapping("/employees/{id}")
    Employee replaceEmployee(@RequestBody Employee newEmpoyee,
                             @PathVariable Long id) {
        return repository.findById(id).map(
                employee -> {
                    employee.setName(newEmpoyee.getName());
                    employee.setRole(newEmpoyee.getRole());
                    return repository.save(employee);
                })
                .orElseGet(() -> {
                    newEmpoyee.setId(id);
                    return repository.save(newEmpoyee);
                });
    }

    @DeleteMapping("employees/{id}")
    void deleteEmployee(@PathVariable Long id) {
        repository.deleteById(id);
    }


}
